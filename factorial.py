def fac(n):
    factorial = 1
    for i in range(2, n + 1):
        factorial *= i
    return factorial


if __name__ == '__main__':
    number = int(input("Введите число для вычисления факториала: "))
    print(fac(number))


